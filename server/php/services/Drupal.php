<?php
/*
    Drupal SWX
    Copyright 2007 Tom Sundström
    All rights reserved.

*/
	/**
	 * SWX Service acting as a wrapper around services provided by the drupal services module.
	 *
	 * @author Tom Sundström
	 **/
	class Drupal
	{
		
		/**
		 * Wrapper
		 *
		 * @param $service The name of the drupal service.
		 * @param $args An array of arguments to the drupal service.
		 * @return an object containing everything the service returns
		 **/
		function callService($method_name, $args)
		{
			return _swx_call_service($method_name, $args);
		}
		
	} // END class 

?>