
DESCRIPTION
-----------
This module provides SWX support to the Services module.


INSTALLATION
------------

1. Extract the module along with the services module into your Drupal modules
   directory.

2. Download SWX PHP 1.01 from http://swxformat.org/download/ and extract it
   into the 'server' folder, so that the path to swx.php is 
   modules/swx/server/php/swx.php

3. Make sure the Drupal.php service that shipped with this module is
   still available at modules/swx/server/php/services/Drupal.php

4. Enable service.module and swx.module at the module admin page
   (admin/build/modules)

5. Configure user permissions for service.module


USAGE
-----

- A simple ActionScript 2.0 call would look something along these lines:

  this.createEmptyMovieClip("loader", this.getNextHighestDepth());
  loader.serviceClass = "Drupal";
  loader.method = "callService";
  loader.args = "['serviceName', ['argument1', 'argument2', 'argument3']";
  loader.debug = true;

  loader.loadMovie("http://www.yourdrupalserver.com/services/swx", "GET");

  function onEnterFrame()
  {
    trace(loader.result);
  }


- In case you wanted to fetch node title and author uid using the node service,
  the loader.args string could look like this:

  loader.args = "['node.load',[1,['title','uid']]]";

  This is a serialized array, where the first argument is the service name
  and the second argument is an array containing parameters to the service.
  In the above example, the first parameter is the node nid and the second
  parameter is an array of fields to be returned.

  to see the results you could do like this:
  trace("title: " + loader.result.title + ", author: " + loader.result);


- For a nice tutorial on how to implement an actionscript client, including better
  event and error handling, check out:
  http://www.aralbalkan.com/1006


- Consult the Services handbook for general info on Drupal services:
  http://drupal.org/handbook/modules/services
